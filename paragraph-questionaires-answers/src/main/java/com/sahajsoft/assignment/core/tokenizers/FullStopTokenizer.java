/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.tokenizers;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class FullStopTokenizer implements Tokenizer {

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.tokenizers.Tokenizer#split(java.lang.
   * StringBuilder)
   */
  @Override
  public List<String> split(String toBeTokezinedString) {
    List<String> sentences = new LinkedList<>();

    try (Scanner scanner = new Scanner(toBeTokezinedString)) {
      scanner.useDelimiter("[.?!]");
      while ( scanner.hasNext() ) {
        sentences.add(scanner.next());
      }
    }
    /*
     * System.out.println("toBeTokezinedString " + toBeTokezinedString +
     * " , sentences are " + sentences);
     */
    return sentences;
  }
}
