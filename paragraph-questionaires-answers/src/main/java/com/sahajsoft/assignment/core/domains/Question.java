/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.domains;

import com.sahajsoft.assignment.core.generators.IDGenerator;
import com.sahajsoft.assignment.core.generators.IDGeneratoryFactory;
import com.sahajsoft.assignment.core.generators.IDGeneratoryFactory.IDGENERATORTYE;
import com.sahajsoft.assignment.core.tokenizers.TokenizerFactory;
import com.sahajsoft.assignment.core.tokenizers.TokenizerFactory.DELIMETERS;

/**
 * The class used as the domain object refered to as question.
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class Question extends Sentence {

  private final String id;

  private final IDGenerator idGenerator = IDGeneratoryFactory.factoryInstance()
      .idGenerator(IDGENERATORTYE.QUESTIONS);

  /**
   * The constructor which takes question as string.
   *
   * @param question The question string.
   */
  public Question(String question) {
    super(TokenizerFactory.instance().instance(DELIMETERS.FULLSTOP)
        .split(question).get(0),
        TokenizerFactory.instance().instance(DELIMETERS.WHITESPACE));
    id = idGenerator.generateID();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if ( obj == null || !(obj instanceof Question) ) {
      return false;

    }
    return this.getOverallSentence()
        .equals(((Question) obj).getOverallSentence());
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return getOverallSentence().hashCode();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.domains.Sentence#toString()
   */
  @Override
  public String toString() {
    return getOverallSentence();
  }

  @Override
  public String id() {
    return id;
  }

}
