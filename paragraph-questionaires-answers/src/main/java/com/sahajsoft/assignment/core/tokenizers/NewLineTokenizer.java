/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.tokenizers;

import java.util.List;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class NewLineTokenizer implements Tokenizer {

  /**
   * 
   */
  NewLineTokenizer() {
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sahajsoft.assignment.core.tokenizers.Tokenizer#split(java.lang.String)
   */
  @Override
  public List<String> split(String toBeTokezinedString) {

    return null;
  }

}
