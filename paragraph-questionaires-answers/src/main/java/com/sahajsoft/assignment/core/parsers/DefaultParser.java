/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.parsers;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.sahajsoft.assignment.core.domains.Answer;
import com.sahajsoft.assignment.core.domains.Comprehension;
import com.sahajsoft.assignment.core.domains.Para;
import com.sahajsoft.assignment.core.domains.Question;
import com.sahajsoft.assignment.core.models.ComprehensionMetaModel;
import com.sahajsoft.assignment.core.tokenizers.Tokenizer;
import com.sahajsoft.assignment.core.tokenizers.TokenizerFactory;
import com.sahajsoft.assignment.core.tokenizers.TokenizerFactory.DELIMETERS;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class DefaultParser implements Parser {

  final ComprehensionMetaModel metaModel;

  /**
   * 
   */
  public DefaultParser(ComprehensionMetaModel metaModel) {
    this.metaModel = metaModel;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.parsers.Parser#parse(java.lang.String)
   */
  @Override
  public Comprehension parse(String toBeParsedString) {

    int lineNo = 1;
    List<Para> paragraphs = new LinkedList<Para>();
    List<Question> questionaires = new LinkedList<Question>();
    List<Answer> answers = new LinkedList<Answer>();

    try (Scanner scanner = new Scanner(toBeParsedString)) {
      scanner.useDelimiter(System.lineSeparator());
      while ( scanner.hasNext() ) {
        // assumes the line has a certain structure
        /*
         * System.out.println(" i is " + metaModel.getParagraphStartLine() +
         * ", " + metaModel.getParagraphEndLine() + ", " +
         * metaModel.getQuestionairesStartLine() + ", " +
         * metaModel.getQuestionairesEndLine());
         */
        if ( lineNo >= metaModel.paraStartLine()
            && lineNo <= metaModel.paraEndLine() ) {
          paragraphs.add(new Para(scanner.next()));
        } else if ( lineNo > metaModel.paraEndLine()
            && lineNo >= metaModel.questionairesStartLine()
            && lineNo <= metaModel.questionairesEndLine() ) {
          questionaires.add(new Question(scanner.next()));
        } else if ( lineNo > metaModel.questionairesEndLine()
            && lineNo >= metaModel.answersStartLine()
            && lineNo <= metaModel.answersEndLine() ) {
          String answerSemiColon = scanner.next();
          Tokenizer tokenizer =
              TokenizerFactory.instance().instance(DELIMETERS.SEMICOLON);
          List<String> splittedAnswers = tokenizer.split(answerSemiColon);
          for ( String individualAnswer : splittedAnswers ) {
            answers.add(new Answer(individualAnswer.trim()));
          }

        }
        lineNo++;
      }
    }
    Comprehension para = new Comprehension(paragraphs, questionaires, answers);
    return para;
  }

  public static void main(String[] args) {

    ComprehensionMetaModel p = new ComprehensionMetaModel();
    DefaultParser parser = new DefaultParser(p);

    parser.parse(new StringBuilder("hi").
        append(System.lineSeparator())
        .append("are")
        .append(System.lineSeparator()).
        append("you")
        .append(System.lineSeparator())
        .append("are1")
        .append(System.lineSeparator())
        .append("are2").
        append(System.lineSeparator())
        .append("are3")
        .append(System.lineSeparator())
        .append("are6").toString());

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sahajsoft.assignment.core.parsers.Parser#parse(java.io.InputStream)
   */
  @Override
  public Comprehension parse(InputStream stream) {
    // TODO Auto-generated method stub
    return null;
  }

}
