/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.tokenizers;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class TokenizerFactory {

  private static final TokenizerFactory instance = new TokenizerFactory();

  public enum DELIMETERS {
    WHITESPACE, NEWLINE, SEMICOLON, FULLSTOP
  };

  /**
   * Intensionaly created private constructor.
   */
  private TokenizerFactory() {

  }

  public static TokenizerFactory instance() {
    return instance;
  }

  /**
   * Creates one of the tokenizer type.
   *
   * @param delimeter The one of the {@link #instance(DELIMETERS)}.
   * @return
   */
  public Tokenizer instance(DELIMETERS delimeter) {
    if ( delimeter == DELIMETERS.WHITESPACE ) {
      return new WhiteSpaceTokenizer();
    }

    if ( delimeter == DELIMETERS.NEWLINE ) {
      return new NewLineTokenizer();
    }

    if ( delimeter == DELIMETERS.SEMICOLON ) {
      return new SemiColonTokenizer();
    }

    if ( delimeter == DELIMETERS.FULLSTOP ) {
      return new FullStopTokenizer();
    }

    return new WhiteSpaceTokenizer();
  }
}
