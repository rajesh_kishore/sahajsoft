/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.examiners;

import com.sahajsoft.assignment.core.domains.Comprehension;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface SentenceExaminer {

  /**
   * @param paragraph
   */
  public Comprehension examineSentence(StringBuilder paragraph);

}
