/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.models;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class ComprehensionMetaModel {

  private int paragraphStartLine = 1;
  private int paragraphEndLine = 1;
  private int questionairesStartLine = 2;
  private int questionairesEndLine = 6;

  private int answersStartLine = 7;
  private int answersEndLine = 7;

  public ComprehensionMetaModel() {

  }

  /**
   * @return the paragraphStartLine
   */
  public final int paraStartLine() {
    return paragraphStartLine;
  }

  /**
   * @return the paragraphEndLine
   */
  public final int paraEndLine() {
    return paragraphEndLine;
  }

  /**
   * @return the questionairesStartLine
   */
  public final int questionairesStartLine() {
    return questionairesStartLine;
  }

  /**
   * @return the questionairesEndLine
   */
  public final int questionairesEndLine() {
    return questionairesEndLine;
  }

  /**
   * @return the answersStartLine
   */
  public final int answersStartLine() {
    return answersStartLine;
  }

  /**
   * @return the answersEndLine
   */
  public final int answersEndLine() {
    return answersEndLine;
  }

}
