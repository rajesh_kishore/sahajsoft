/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.configurations;

import com.sahajsoft.assignment.core.engine.Configuration;
import com.sahajsoft.assignment.extensions.spi.ComprehensionSolverProvider;

/**
 * The class is the builder class for creating configuration object. This is
 * primary consumer facing class. <code>
 * ConfigurationBuilder configurationBuilder =
        ConfigurationBuilder.defaultConfigurationBuilder();
 *          
 * </code>
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class ConfigurationBuilder {

  /**
   * The default provider class name for {@link ComprehensionSolverProvider}
   */
  public static final String DEFAULT_COMPREHENSION_SOLVER_PROVIDER =
      "com.sahajsoft.assignment.extensions.spi.impl.defaultprovider"
          + ".DefaultComprehensionSolverProvider";

  /**
   * The default configuration builder.
   */
  private static final ConfigurationBuilder DEFAULT_CONFIGURATION_BUILDER;

  static {
    DEFAULT_CONFIGURATION_BUILDER =
        new ConfigurationBuilder(DEFAULT_COMPREHENSION_SOLVER_PROVIDER);
  }

  /**
   * The implementor provider class name for {@link ComprehensionSolverProvider}
   * .
   */
  final private String comprehensionSolverProvider;

  /**
   * The default and only constructor visible to consumer, which expects the
   * implementor provider class name for {@link ComprehensionSolverProvider} ,
   * consumer can use the Configuration with default object using
   * {@link #buildConfiguration()}.
   * 
   * @param comprehensionSolverProvider The class name for provider.
   */
  public ConfigurationBuilder(String
      comprehensionSolverProvider) {
    this.comprehensionSolverProvider =
        comprehensionSolverProvider;
  }

  /**
   * @return the comprehensionSolverProvider
   */
  public final String comprehensionSolverProvider() {
    return comprehensionSolverProvider;
  }

  /**
   * Returns the default configuration instance.
   * 
   * @return The default {@link Configuration} instance.
   */
  public Configuration defaultConfiguration() {
    return new Configuration(DEFAULT_CONFIGURATION_BUILDER);
  }

  /**
   * Builds the configuration.
   * 
   * @return The {@link Configuration} instance.
   */
  public Configuration buildConfiguration() {
    return new Configuration(this);
  }

  /**
   * The default configuration builder.
   * 
   * @return the defaultConfigurationBuilder
   */
  public static final ConfigurationBuilder defaultConfigurationBuilder() {
    return DEFAULT_CONFIGURATION_BUILDER;
  }

}
