/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.examiners;

import com.sahajsoft.assignment.core.domains.Comprehension;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class FullStopBasedSentenceExaminer implements SentenceExaminer {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sahajsoft.assignment.core.examiners.SentenceExaminer#examineSentence
   * (java.lang.StringBuilder)
   */
  @Override
  public Comprehension examineSentence(StringBuilder paragraph) {
    // TODO Auto-generated method stub
    return null;
  }

}
