/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.generators;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
abstract class DefaultIDGenerator extends IDGenerator {

  protected AtomicLong defaultID = new AtomicLong(1);

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.generators.IDGenerator#id()
   */
  @Override
  public String generateID() {
    return String.valueOf(defaultID.getAndIncrement());
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.generators.IDGenerator#currentID()
   */
  @Override
  public String currentID() {
    return String.valueOf(defaultID.get());
  }

}
