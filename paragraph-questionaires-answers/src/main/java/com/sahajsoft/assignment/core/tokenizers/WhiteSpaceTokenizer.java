/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.tokenizers;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class WhiteSpaceTokenizer implements Tokenizer {

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.tokenizers.Tokenizer#split(java.lang.
   * StringBuilder)
   */
  @Override
  public List<String> split(String toBeTokezinedString) {

    List<String> words = new LinkedList<>();
    /*
     * BreakIterator boundary = BreakIterator.getWordInstance(Locale.US);
     * boundary.setText(toBeTokezinedString); int start = boundary.first(); for
     * ( int end = boundary.next(); end != BreakIterator.DONE; start = end, end
     * = boundary.next() ) { words.add(toBeTokezinedString.substring(start,
     * end)); }
     */
    String[] word = toBeTokezinedString.split("\\s+");
    for ( int i = 0; i < word.length; i++ ) {
      words.add(word[i].replaceAll("[^\\w]", ""));
    }

    return words;
  }
}
