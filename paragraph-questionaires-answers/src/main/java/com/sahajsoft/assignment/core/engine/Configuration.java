/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.engine;

import com.sahajsoft.assignment.common.loggers.LoggerAdapter;
import com.sahajsoft.assignment.core.configurations.ConfigurationBuilder;
import com.sahajsoft.assignment.core.exceptions.ProviderInstantiationException;
import com.sahajsoft.assignment.core.models.ComprehensionMetaModel;
import com.sahajsoft.assignment.extensions.spi.ComprehensionSolverProvider;

/**
 * The class contains the raw configuration provided by client. <code>
 * Configuration configuration =
          ConfigurationBuilder.defaultConfigurationBuilder().
          buildConfiguration();
   * or
   * new ConfigurationBuilder(
              ).
              buildConfiguration();
    </code>
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class Configuration {

  /**
   * The solver provider for comprehension.
   */
  private ComprehensionSolverProvider solverProvider;

  /**
   * The logger.
   */
  private static final LoggerAdapter loggerAdapter = LoggerAdapter
      .getInstance();

  /**
   * The only constructor which is exposed to create configurations.
   *
   * @param builder The {@link ConfigurationBuilder}
   */
  public Configuration(ConfigurationBuilder builder) {
    try {
      solverProvider =
          (ComprehensionSolverProvider) (Class.forName(builder
              .comprehensionSolverProvider())).newInstance();
    } catch ( InstantiationException | IllegalAccessException
        | ClassNotFoundException e ) {
      loggerAdapter.logSevere("PROVIDER_CLASS_INSTANTIATION_ERROR", e);
      throw new ProviderInstantiationException(e);
    }
  }

  /**
   * Explicit private constructor so that there is only one way to create
   * {@link Configuration}.
   */
  private Configuration() {
  }

  /**
   * The method creates the {@link ComprehensionProcessorEngine} instance given
   * configuration passed by the client.
   * 
   * @return The instance of {@link ComprehensionProcessorEngine}
   */
  public ComprehensionProcessorEngine createComprehensionProcessorEngine() {
    return new ComprehensionProcessorEngine(this);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder(50);
    return builder.toString();
  }

  /**
   * Returns the meta data about comprehension such as para start line etc.
   *
   * @return The meta data about comprehension structure.
   */
  ComprehensionMetaModel paraMetaModel() {
    return new ComprehensionMetaModel();
  }

  /**
   * Returns the comprehension solver provider.
   *
   * @return The solver provider
   */
  ComprehensionSolverProvider comprehensionSolverProvider() {
    return solverProvider;
  }

}
