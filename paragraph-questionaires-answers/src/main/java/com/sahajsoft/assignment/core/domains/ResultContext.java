/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.domains;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface ResultContext {

  /**
   * Adds the answer to the result set. It keeps the question in sorted order.
   *
   * @param forQuestionID The question id for which answer is found.
   * @param ans The answer.
   */
  public abstract void addAnswer(String forQuestionID, Answer ans);

  /**
   * The result as per system ask.
   * 
   * @return
   */
  public abstract String result();

}