/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.domains;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.sahajsoft.assignment.core.generators.IDGenerator;
import com.sahajsoft.assignment.core.generators.IDGeneratoryFactory;
import com.sahajsoft.assignment.core.generators.IDGeneratoryFactory.IDGENERATORTYE;
import com.sahajsoft.assignment.core.tokenizers.Tokenizer;

/**
 * The lowest level entity.
 *
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class Sentence {

  private final String overallSentence;

  private final List<String> wordsSeperated = new LinkedList<>();

  private final String id;

  private final IDGenerator idGenerator = IDGeneratoryFactory.factoryInstance()
      .idGenerator(IDGENERATORTYE.SENTENCE);

  /**
   * @param s
   * @param tokenizer TODO
   */
  public Sentence(String s, Tokenizer tokenizer) {
    this.overallSentence = s;
    id = idGenerator.generateID();
    // System.out.println("senetence is " + s);
    // Tokenizer tokenizer = new WhiteSpaceTokenizer();
    if ( null != tokenizer ) {
      wordsSeperated.addAll(tokenizer.split(s));
    }

    System.out.println("words " + wordsSeperated);
    // Tokenizer sentenceTokenizer = new WhiteSpaceTokenizer();
    // sentenceLst.forEach(s -> sentences.add(new Sentence(s,
    // sentenceTokenizer)));

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if ( obj == null || !(obj instanceof Sentence) ) {
      return false;

    }
    return this.getOverallSentence().equals(
        ((Sentence) obj).getOverallSentence());
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return overallSentence.hashCode();
  }

  /**
   * @return the overallSentence
   */
  public final String getOverallSentence() {
    return overallSentence;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return overallSentence;
  }

  /**
   * Returns the unmodifiable words set.
   *
   * @return the wordsSeperated
   */
  public List<String> wordsSeperated() {
    return Collections.unmodifiableList(wordsSeperated);
  }

  /**
   * The sentence id.
   *
   * @return The id.
   */
  public String id() {
    return id;
  }

}
