/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.generators;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
class SentenceIDGenerator extends IDGenerator {

  private final static String DEFAULT_PREFIX = "S";

  private IDGenerator idGenerator = new DefaultIDGenerator() {
  };

  private static final IDGenerator sentenceIDGenerator =
      new SentenceIDGenerator();

  /**
   * The default private constructor to avoid explicit instantiation.
   */
  private SentenceIDGenerator() {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.generators.DefaultIDGenerator#id()
   */
  @Override
  public String generateID() {
    return DEFAULT_PREFIX + idGenerator.generateID();
  }

  static IDGenerator getInstance() {
    return sentenceIDGenerator;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.generators.IDGenerator#defaultPrefix()
   */
  @Override
  public String defaultPrefix() {
    return DEFAULT_PREFIX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.generators.IDGenerator#currentID()
   */
  @Override
  public String currentID() {
    return idGenerator.currentID();
  }
}
