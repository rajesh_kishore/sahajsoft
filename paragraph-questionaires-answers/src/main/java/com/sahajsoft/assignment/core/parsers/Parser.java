/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.parsers;

import java.io.InputStream;

import com.sahajsoft.assignment.core.domains.Comprehension;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface Parser {

  public Comprehension parse(String toBeParsedString);

  public Comprehension parse(InputStream stream);

}
