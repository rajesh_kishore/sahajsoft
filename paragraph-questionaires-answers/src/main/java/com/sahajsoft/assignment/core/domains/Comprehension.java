/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.domains;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The class used as the domain object.
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class Comprehension {

  /**
   * The list of paragraph, ideally 1.
   */
  private final List<Para> para = new LinkedList<Para>();
  /**
   * The list of questions parsed.
   */
  private final List<Question> questions = new LinkedList<Question>();

  /**
   * The map of answer id and answer.
   */
  private final Map<String, Answer> answers =
      new LinkedHashMap<String, Answer>();

  /**
   * @param paragraphs
   * @param questionaires
   * @param answers
   */
  public Comprehension(List<Para> paragraphs, List<Question> questionaires,
      List<Answer> answers) {

    /*
     * paragraphs.forEach(p -> para.add(new Para(p))); questionaires.forEach(q
     * -> questions.add(new Question(q))); answers2.forEach(ans ->
     * answers.add(new Answer(ans)));
     */
    this.para.addAll(paragraphs);
    this.questions.addAll(questionaires);

    answers.forEach(a -> this.answers.put(a.id(), a));
    /*
     * this.answers.putAll(answers2.stream().collect(Collectors.toMap(Answer::id,
     * Function.identity())) );
     */
  }

  /**
   * Retruns the unmodifiable list of paragraphs, ideally 1
   *
   * @return The para list.
   */
  public List<Para> paras() {
    return Collections.unmodifiableList(para);
  }

  /**
   * Returns The unmodifiable list of questions.
   *
   * @return The list of questions.
   */
  public List<Question> questions() {
    return Collections.unmodifiableList(questions);
  }

  /**
   * Returns the unmodifiable list of answers.
   *
   * @return The list of answers.
   */
  public List<Answer> answers() {
    return Collections.unmodifiableList(new LinkedList<>(answers.values()));
  }

  /**
   * Returns the answer given answer id.
   * 
   * @param answerID The answer id.
   * @return The Answer.
   */
  public Answer answerById(String answerID) {
    return answers.get(answerID);
  }
}
