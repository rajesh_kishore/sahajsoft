/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.tokenizers;

import java.util.List;

/**
 * Splits the string considering the token.
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface Tokenizer {

  /**
   * @param toBeTokezinedString The string to be tokenized.
   * @return The unmodifiable list of splitted string.
   */
  public List<String> split(String toBeTokezinedString);

}
