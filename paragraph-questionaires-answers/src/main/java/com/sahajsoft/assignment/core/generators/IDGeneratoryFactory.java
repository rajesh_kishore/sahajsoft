/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.generators;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class IDGeneratoryFactory {

  private static final IDGeneratoryFactory factory = new IDGeneratoryFactory();

  public enum IDGENERATORTYE {
    SENTENCE, QUESTIONS, ANSWERS
  };

  public static IDGeneratoryFactory factoryInstance() {
    return factory;
  }

  public IDGenerator idGenerator(IDGENERATORTYE type) {
    if ( type == IDGENERATORTYE.SENTENCE ) {
      return SentenceIDGenerator.getInstance();
    }

    if ( type == IDGENERATORTYE.QUESTIONS ) {
      return QuestionIDGenerator.getInstance();
    }

    if ( type == IDGENERATORTYE.ANSWERS ) {
      return AnswerIDGenerator.getInstance();
    }

    return new DefaultIDGenerator() {
    };
  }

}
