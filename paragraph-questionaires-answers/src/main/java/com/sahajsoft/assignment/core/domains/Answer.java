/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.domains;

import com.sahajsoft.assignment.core.generators.IDGenerator;
import com.sahajsoft.assignment.core.generators.IDGeneratoryFactory;
import com.sahajsoft.assignment.core.generators.IDGeneratoryFactory.IDGENERATORTYE;
import com.sahajsoft.assignment.core.tokenizers.TokenizerFactory;
import com.sahajsoft.assignment.core.tokenizers.TokenizerFactory.DELIMETERS;

/**
 * The answer class used as the domain object.
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class Answer extends Sentence {

  private final String id;

  private final IDGenerator idGenerator = IDGeneratoryFactory.factoryInstance()
      .idGenerator(IDGENERATORTYE.ANSWERS);

  /**
   * @param s
   */
  public Answer(String s) {
    super(s, TokenizerFactory.instance().instance(DELIMETERS.SEMICOLON));
    id = idGenerator.generateID();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if ( obj == null || !(obj instanceof Answer) ) {
      return false;

    }
    return super.getOverallSentence()
        .equals(((Answer) obj).getOverallSentence());
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return getOverallSentence().hashCode();
  }

  @Override
  public String id() {
    return id;
  }

}
