/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.domains;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sahajsoft.assignment.core.tokenizers.Tokenizer;
import com.sahajsoft.assignment.core.tokenizers.TokenizerFactory;
import com.sahajsoft.assignment.core.tokenizers.TokenizerFactory.DELIMETERS;

/**
 * The class used as the domain object represents individual paragraph.
 *
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class Para {

  private final String overAllParagraphs;
  private final Map<String, Sentence> sentences =
      new LinkedHashMap<String, Sentence>();

  /**
   * @param next
   */
  public Para(String para) {
    this.overAllParagraphs = para;
    Tokenizer tokenizer =
        TokenizerFactory.instance().instance(DELIMETERS.FULLSTOP);
    List<String> sentenceLst = tokenizer.split(overAllParagraphs);

    sentenceLst.forEach(s -> {
      Sentence sent = new Sentence(s,
          TokenizerFactory.instance().instance(DELIMETERS.WHITESPACE));
      sentences.put(sent.id(), sent);
    });

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return overAllParagraphs;
  }

  /**
   * Returns the unmodifiable sentences list.
   *
   * @return
   */
  public List<Sentence> sentences() {
    return Collections.unmodifiableList(new LinkedList<>(sentences.values()));
  }

  /**
   * Returns the Sentence given number in a para.
   *
   * @param number the sentence number.
   * @return The {@link Sentence}
   */
  public Sentence sentenceByNumber(String number) {
    return sentences.get(number);
  }

}
