/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.generators;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
class AnswerIDGenerator extends IDGenerator {

  private final static String DEFAULT_PREFIX = "A";

  private IDGenerator idGenerator = new DefaultIDGenerator() {
  };

  private static final IDGenerator answerIDGenerator =
      new AnswerIDGenerator();

  /**
   * The default private constructor to avoid explicit instantiation.
   */
  private AnswerIDGenerator() {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.generators.DefaultIDGenerator#id()
   */
  @Override
  public String generateID() {
    return defaultPrefix() + idGenerator.generateID();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.generators.IDGenerator#defaultPrefix()
   */
  @Override
  public String defaultPrefix() {
    return DEFAULT_PREFIX;
  }

  static IDGenerator getInstance() {
    return answerIDGenerator;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.generators.IDGenerator#currentID()
   */
  @Override
  public String currentID() {
    return idGenerator.currentID();
  }
}
