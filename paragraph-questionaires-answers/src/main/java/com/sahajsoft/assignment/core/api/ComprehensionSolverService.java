/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.api;

import com.sahajsoft.assignment.core.domains.Comprehension;
import com.sahajsoft.assignment.core.domains.ResultContext;

/**
 * The interface which defines the basic api to solve the comprehension. The
 * implementation can be hooked by different service provider.
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface ComprehensionSolverService {

  /**
   * Solves the comprehension and returns the result.
   *
   * @param comprehension The complete comprhension including questionaires and
   *          answers.
   * @return The result.
   */
  public ResultContext solveComprehension(Comprehension comprehension);

}
