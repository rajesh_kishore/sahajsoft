/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.engine;

import java.io.InputStream;

import com.sahajsoft.assignment.core.api.ComprehensionSolverService;
import com.sahajsoft.assignment.core.domains.Comprehension;
import com.sahajsoft.assignment.core.domains.ResultContext;
import com.sahajsoft.assignment.core.parsers.DefaultParser;
import com.sahajsoft.assignment.core.parsers.Parser;

/**
 * The main class which does all the processing , all the request would be
 * routed through this class only.
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class ComprehensionProcessorEngine {

  /**
   * The parser.
   */
  private final Parser parser;

  /**
   * The service resolved by the provider which does solve the comprehension.
   */
  private final ComprehensionSolverService service;

  /**
   * The configuration.
   */
  private final Configuration configuration;

  /**
   * The non public constructor.
   * 
   * @param configuration The configuration.
   */
  ComprehensionProcessorEngine(Configuration configuration) {
    this.configuration = configuration;
    parser = new DefaultParser(configuration.paraMetaModel());
    service =
        this.configuration.comprehensionSolverProvider()
            .comprehensionSolverService();
  }

  /**
   * Submits the comprehension for processing and returns the result.
   *
   * @param paragraph The paragraph.
   * @return The result context.
   */
  public ResultContext submit(String paragraph) {
    Comprehension comprehension = parser.parse(paragraph);
    return service.solveComprehension(comprehension);
  }

  /**
   * Submits the comprehension for processing and returns the result.
   *
   * @param paragraphIS The inputStream.
   * @return The result context.
   */
  public ResultContext submit(InputStream paragraphIS) {
    // TODO:
    return null;
  }
}
