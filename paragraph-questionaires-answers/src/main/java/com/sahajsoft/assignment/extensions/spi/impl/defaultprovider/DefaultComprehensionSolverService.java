/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.extensions.spi.impl.defaultprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.sahajsoft.assignment.core.api.ComprehensionSolverService;
import com.sahajsoft.assignment.core.domains.Comprehension;
import com.sahajsoft.assignment.core.domains.Question;
import com.sahajsoft.assignment.core.domains.ResultContext;
import com.sahajsoft.assignment.core.domains.Sentence;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class DefaultComprehensionSolverService implements
    ComprehensionSolverService {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sahajsoft.assignment.core.api.ComprehensionSolverService#solveComprehension
   * (com.sahajsoft.assignment.core.domains.Comprehension)
   */
  @Override
  public ResultContext solveComprehension(Comprehension comprehension) {
    // Comprehension comprehension = parser.parse(paragraph);
    final ResultContext result = new DefaultResult();

    final Map<String, LinkedList<String>> answersInProbableSentences =
        new HashMap<String, LinkedList<String>>();

    findProbableMatchingSentences(comprehension, answersInProbableSentences);

    Map<String, LinkedHashMap<String, Integer>> answersInProbableQuestion =
        new HashMap<String, LinkedHashMap<String, Integer>>();

    findProbableQuestionsPerAnswer(comprehension, answersInProbableSentences,
        answersInProbableQuestion);

    sortProbableQuestionsByRankPerAnswer(answersInProbableQuestion);

    final Map<String, String> quesByAns = new LinkedHashMap<>();
    // {A1={Q2=5, Q3=1, Q4=5, Q5=13}, A3={Q3=3, Q4=1, Q5=1}, A5={Q1=2, Q2=1,
    // Q3=1, Q4=2, Q5=5}}
    determineQuestionByAnswer(answersInProbableQuestion, quesByAns);

    // add the result
    for ( Entry<String, String> quesAnsNo : quesByAns.entrySet() ) {
      result.addAnswer(quesAnsNo.getKey(),
          comprehension.answerById(quesAnsNo.getValue()));
    }

    return result;
  }

  /**
   * @param answersInProbableQuestion
   * @param quesByAns
   */
  private void determineQuestionByAnswer(
      Map<String, LinkedHashMap<String, Integer>> answersInProbableQuestion,
      Map<String, String> quesByAns) {
    for ( Entry<String, LinkedHashMap<String, Integer>> entry : answersInProbableQuestion
        .entrySet() ) {
      LinkedHashMap<String, Integer> questionAndRankProbability =
          entry.getValue();
      if ( questionAndRankProbability.isEmpty() ) {
        continue;
      }
      boolean highestRankQuestion = true;
      for ( Entry<String, Integer> answerHighestMatchingQuestion : questionAndRankProbability
          .entrySet() ) {
        highestRankQuestion = true;
        for ( Entry<String, LinkedHashMap<String, Integer>> otherEntry : answersInProbableQuestion
            .entrySet() ) {
          if ( otherEntry.getKey().equals(entry.getKey()) ) {
            continue;
          }
          LinkedHashMap<String, Integer> otherQuestionAndRankProbability =
              otherEntry.getValue();
          if ( otherQuestionAndRankProbability.isEmpty() ) {
            continue;
          }
          if ( answerHighestMatchingQuestion.getValue().compareTo(
              otherQuestionAndRankProbability.getOrDefault(
                  answerHighestMatchingQuestion.getKey(),
                  Integer.valueOf(0))) < 0 ) {
            highestRankQuestion = false;
            break;
          }
        }
        if ( highestRankQuestion ) {
          quesByAns.put(answerHighestMatchingQuestion.getKey(), entry.getKey());
          break;
        }
      }
    }
  }

  /**
   * @param answersInProbableQuestion
   */
  private void sortProbableQuestionsByRankPerAnswer(
      Map<String, LinkedHashMap<String, Integer>> answersInProbableQuestion) {
    for ( Entry<String, LinkedHashMap<String, Integer>> entry : answersInProbableQuestion
        .entrySet() ) {
      LinkedHashMap<String, Integer> map = entry.getValue();

      if ( map.isEmpty() ) {
        continue;
      }
      Comparator<Entry<String, Integer>> valueComparator =
          new Comparator<Entry<String, Integer>>()
          {
            @Override
            public int compare(Entry<String, Integer> e1,
                Entry<String, Integer> e2) {
              Integer v1 = e1.getValue();
              Integer v2 = e2.getValue();
              return v2.compareTo(v1);
            }
          };
      List<Entry<String, Integer>> listOfEntries =
          new ArrayList<Entry<String, Integer>>(map.entrySet());

      Collections.sort(listOfEntries, valueComparator);
      map.clear();
      listOfEntries.forEach(e -> map.put(e.getKey(), e.getValue()));
    }
  }

  /**
   * @param comprehension
   * @param answersInProbableSentences
   * @param answersInProbableQuestion
   */
  private void findProbableQuestionsPerAnswer(Comprehension comprehension,
      Map<String, LinkedList<String>> answersInProbableSentences,
      Map<String, LinkedHashMap<String, Integer>> answersInProbableQuestion) {
    for ( Entry<String, LinkedList<String>> entry : answersInProbableSentences
        .entrySet() ) {
      for ( String sentenceNo : entry.getValue() ) {
        Sentence sentence =
            comprehension.paras().get(0).sentenceByNumber(sentenceNo);
        for ( Question question : comprehension.questions() ) {
          final List<String> copiedSentenceWords =
              new LinkedList<>(sentence.wordsSeperated());
          copiedSentenceWords.retainAll(question.wordsSeperated());
          if ( !answersInProbableQuestion.containsKey(entry.getKey()) ) {
            answersInProbableQuestion.put(entry.getKey(),
                new LinkedHashMap<>());
          }
          LinkedHashMap<String, Integer> map =
              answersInProbableQuestion.get(entry.getKey());
          if ( copiedSentenceWords.size() > 0 ) {
            map.put(question.id(), copiedSentenceWords.size());
          }
        }
      }
    }
  }

  /**
   * @param comprehension
   * @param answersInProbableSentences
   */
  private void findProbableMatchingSentences(Comprehension comprehension,
      Map<String, LinkedList<String>> answersInProbableSentences) {
    comprehension.answers().forEach(a -> {
      comprehension.paras().get(0).sentences().forEach(s -> {
        if ( s.getOverallSentence().contains(a.getOverallSentence()) ) {
          if ( !answersInProbableSentences.containsKey(a.id()) ) {
            answersInProbableSentences.put(a.id(), new LinkedList<>());
          }
          answersInProbableSentences.get(a.id()).add(s.id());
        }
      });
    });
  }

}
