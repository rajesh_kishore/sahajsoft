/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.extensions.spi.impl.defaultprovider;

import com.sahajsoft.assignment.core.api.ComprehensionSolverService;
import com.sahajsoft.assignment.extensions.spi.ComprehensionSolverProvider;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class DefaultComprehensionSolverProvider implements
    ComprehensionSolverProvider {

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.extensions.spi.ComprehensionSolverProvider#
   * comprehensionSolverService()
   */
  @Override
  public ComprehensionSolverService comprehensionSolverService() {
    return new DefaultComprehensionSolverService();
  }

}
