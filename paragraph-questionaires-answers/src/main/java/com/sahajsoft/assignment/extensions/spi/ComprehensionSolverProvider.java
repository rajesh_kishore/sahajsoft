/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.extensions.spi;

import com.sahajsoft.assignment.core.api.ComprehensionSolverService;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface ComprehensionSolverProvider {

  public ComprehensionSolverService comprehensionSolverService();

}
