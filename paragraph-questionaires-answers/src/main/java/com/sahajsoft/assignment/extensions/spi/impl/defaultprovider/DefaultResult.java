/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.extensions.spi.impl.defaultprovider;

import java.util.Map;
import java.util.TreeMap;

import com.sahajsoft.assignment.core.domains.Answer;
import com.sahajsoft.assignment.core.domains.ResultContext;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class DefaultResult implements ResultContext {

  private final Map<Integer, Answer> answers;

  /**
   * @param size
   */
  public DefaultResult() {
    answers = new TreeMap<Integer, Answer>();
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sahajsoft.assignment.core.domains.ResultContext#addAnswer(java.lang
   * .String, com.sahajsoft.assignment.core.domains.Answer)
   */
  @Override
  public void addAnswer(String forQuestionID, Answer ans) {
    answers
        .put(
            Integer.valueOf(forQuestionID.substring(forQuestionID.indexOf("Q") + 1)),
            ans);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sahajsoft.assignment.core.domains.ResultContext#result()
   */
  @Override
  public String result() {
    StringBuilder sb = new StringBuilder();
    answers.entrySet().forEach(e -> {
      sb.append("" + e.getValue().getOverallSentence());
      sb.append(System.lineSeparator());
    });
    System.out.println(answers);
    return sb.toString();
  }
}
