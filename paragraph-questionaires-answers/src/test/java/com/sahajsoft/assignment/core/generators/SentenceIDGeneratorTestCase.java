/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.generators;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.testng.Assert;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class SentenceIDGeneratorTestCase {

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  /**
   * Test method for
   * {@link com.sahajsoft.assignment.core.generators.SentenceIDGenerator#generateID()}
   * .
   */
  @Test
  public void testGenerateID() {

    Integer id = Integer.valueOf(Integer.parseInt(SentenceIDGenerator
        .getInstance().currentID()));
    Assert.assertEquals(
        SentenceIDGenerator.getInstance().generateID(),
        "S"
            + id);
    id = Integer.valueOf(Integer.parseInt(SentenceIDGenerator
        .getInstance().currentID()));
    Assert.assertEquals(
        SentenceIDGenerator.getInstance().generateID(),
        "S"
            + id);
    id = Integer.valueOf(Integer.parseInt(SentenceIDGenerator
        .getInstance().currentID()));
    Assert.assertEquals(
        SentenceIDGenerator.getInstance().generateID(),
        "S"
            + id);
  }

  /**
   * Test method for
   * {@link com.sahajsoft.assignment.core.generators.IDGenerator#defaultPrefix()}
   * .
   */
  @Test
  public void testDefaultPrefix() {
    Assert.assertEquals(SentenceIDGenerator.getInstance().defaultPrefix(), "S");
  }

}
