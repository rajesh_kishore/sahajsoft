/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.parsers;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.testng.Assert;

import com.sahajsoft.assignment.core.domains.Comprehension;
import com.sahajsoft.assignment.core.models.ComprehensionMetaModel;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class DefaultParserTest {

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  /**
   * Test method for
   * {@link com.sahajsoft.assignment.core.parsers.DefaultParser#parse(java.lang.String)}
   * .
   */
  @Test
  public void testParseString() {
    Parser parser = new DefaultParser(new ComprehensionMetaModel());
    Comprehension para =
        parser.parse(new StringBuilder(
            "Ram is a good boy.He studies in class 10th.").
            append(System.lineSeparator())
            .append("are?")
            .append(System.lineSeparator()).
            append("you?")
            .append(System.lineSeparator())
            .append("are1?")
            .append(System.lineSeparator())
            .append("are2?").
            append(System.lineSeparator())
            .append("are3?")
            .append(System.lineSeparator())
            .append("are6;ans7;ans8").toString());

    List<String> para1 = new LinkedList<String>();
    List<String> questions = new LinkedList<String>();

    List<String> answers = new LinkedList<String>();

    para1.add("Ram is a good boy.He studies in class 10th.");
    questions.add("are");
    questions.add("you");
    questions.add("are1");
    questions.add("are2");
    questions.add("are3");
    answers.add("are6;ans7;ans8");

    List<String> answers1 = new LinkedList<String>();
    answers1.add("are6");
    answers1.add("ans7");
    answers1.add("ans8");

    para.paras().forEach(
        a -> Assert.assertEquals(para1.get(0), a.toString()));

    List<String> passedParaSentences = new LinkedList<>();
    passedParaSentences.add("Ram is a good boy");
    passedParaSentences.add("He studies in class 10th");

    List<String> resultParaSentences = new LinkedList<>();
    para.paras().get(0).sentences()
        .forEach(s -> resultParaSentences.add(s.getOverallSentence()));

    Assert.assertEquals(resultParaSentences, passedParaSentences);

    List<String> passedParaSentenceWords = new LinkedList<>();
    passedParaSentenceWords
        .addAll(Arrays.asList("Ram is a good boy".split("\\s+")));
    List<String> resultParaSentenceWords = new LinkedList<>();
    resultParaSentenceWords.addAll(para.paras().get(0).sentences().get(0)
        .wordsSeperated());

    Assert.assertEquals(resultParaSentenceWords, passedParaSentenceWords);

    List<String> resultQuestion = new LinkedList<>();
    para.questions().forEach(a -> resultQuestion.add(a.toString()));
    Assert.assertEquals(questions, resultQuestion);

    List<String> resultAnswer = new LinkedList<>();
    para.answers().forEach(a -> {
      resultAnswer.add(a.toString());
      // System.out.println(a.toString());
      });
    Assert.assertEquals(resultAnswer, answers1);

  }
}
