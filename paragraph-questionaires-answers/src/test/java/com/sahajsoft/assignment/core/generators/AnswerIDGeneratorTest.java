/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment.core.generators;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.testng.Assert;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class AnswerIDGeneratorTest {

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  /**
   * Test method for
   * {@link com.sahajsoft.assignment.core.generators.AnswerIDGenerator#generateID()}
   * .
   */
  @Test
  public void testGenerateID() {

    Integer id =
        Integer.valueOf(Integer.parseInt(AnswerIDGenerator.getInstance()
            .currentID()));
    Assert.assertEquals(
        AnswerIDGenerator.getInstance().generateID(),
        "A"
            + id);
    id =
        Integer.valueOf(Integer.parseInt(AnswerIDGenerator.getInstance()
            .currentID()));
    Assert.assertEquals(
        AnswerIDGenerator.getInstance().generateID(),
        "A"
            + id);
    id =
        Integer.valueOf(Integer.parseInt(AnswerIDGenerator.getInstance()
            .currentID()));
    Assert.assertEquals(
        AnswerIDGenerator.getInstance().generateID(),
        "A"
            + id);

  }

  /**
   * Test method for
   * {@link com.sahajsoft.assignment.core.generators.AnswerIDGenerator#defaultPrefix()}
   * .
   */
  @Test
  public void testDefaultPrefix() {
    Assert.assertEquals(AnswerIDGenerator.getInstance().defaultPrefix(), "A");
  }

}
