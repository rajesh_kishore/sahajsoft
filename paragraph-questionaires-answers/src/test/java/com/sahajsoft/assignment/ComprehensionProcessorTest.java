/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.sahajsoft.assignment;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.testng.Assert;

import com.sahajsoft.assignment.core.configurations.ConfigurationBuilder;
import com.sahajsoft.assignment.core.domains.ResultContext;
import com.sahajsoft.assignment.core.engine.ComprehensionProcessorEngine;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class ComprehensionProcessorTest {

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  /**
   * Test method for
   * {@link com.sahajsoft.assignment.core.engine.ComprehensionProcessorEngine#submit(java.lang.String)}
   * .
   */
  @Test
  public void testSubmit() {
    ComprehensionProcessorEngine processor =
        ConfigurationBuilder.defaultConfigurationBuilder().buildConfiguration()
            .createComprehensionProcessorEngine();
    StringBuilder sb = new StringBuilder();
    sb.append(
        "Zebras are several species of African equids (horse family) united by their distinctive black and white stripes. Their stripes come in different patterns, unique to each individual. They are generally social animals that live in small harems to large herds. Unlike their closest relatives, horses and donkeys, zebras have never been truly domesticated. There are three species of zebras: the plains zebra,the Gr�vy's zebra and the mountain zebra. The plains zebra and the mountain zebra belong to the subgenus Hippotigris, but Gr�vy's zebra is the sole species of subgenus Dolichohippus. The latter resembles an ass, to which it is closely related, while the former two are more horse-like. All three belong to the genus Equus, along with other living equids. The unique stripes of zebras make them one of the animals most familiar to people. They occur in a variety of habitats,such as grasslands, savannas, woodlands, thorny scrublands,mountains, and coastal hills. However, various anthropogenic factors have had a severe impact on zebra populations, in particular hunting for skins and habitat destruction. Gr�vy's zebra and the mountain zebra are endangered. While plains zebras are much more plentiful,one subspecies - the Quagga - became extinct in the late 19th century. Though there is currently a plan, called the Quagga Project,that aims to breed zebras that are phenotypically similar to the Quagga, in a process called breeding back.")
        .
        append(System.lineSeparator())
        .append("Which Zebras are endangered?")
        .
        append(System.lineSeparator())
        .append("What is the aim of the Quagga Project?")
        .
        append(System.lineSeparator())
        .append("Which animals are some of their closest relatives?")
        .
        append(System.lineSeparator())
        .append("Which are the three species of zebras?")
        .
        append(System.lineSeparator())
        .append(
            "Which subgenus do the plains zebra and the mountain zebra belong to?")
        .
        append(System.lineSeparator())
        .append(
            "subgenus Hippotigris;the plains zebra,the Gr�vy's zebra and the mountain zebra;horses and donkeys;aims to breed zebras that are phenotypically similar to the Quagga; Gr�vy's zebra and the mountain zebra");
    ResultContext result = processor.submit(sb.toString());
    System.out.println(result.result());
    StringBuilder expectedAnswerOrder = new StringBuilder();
    expectedAnswerOrder
        .append("Gr�vy's zebra and the mountain zebra")
        .append(System.lineSeparator())
        .
        append(
            "aims to breed zebras that are phenotypically similar to the Quagga")
        .append(System.lineSeparator()).
        append("horses and donkeys").append(System.lineSeparator()).
        append("the plains zebra,the Gr�vy's zebra and the mountain zebra")
        .append(System.lineSeparator()).
        append("subgenus Hippotigris").append(System.lineSeparator());
    Assert.assertEquals(result.result(), expectedAnswerOrder.toString());

  }
}
